kxmlrpcclient (5.115.0-3) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.7.1, no changes required.
  * Remove the unused debian/meta/ stuff.
  * CI: simplify/improve config.
  * Modernize building:
    - add the dh-sequence-kf5 build dependency to use the kf5 addon
      automatically
    - add the dh-sequence-pkgkde-symbolshelper build dependency to use the
      pkgkde_symbolshelper automatically
    - drop the pkg-kde-tools build dependency, no more explicitly needed
    - drop all the manually specified addons and buildsystem for dh
  * Drop old breaks/replaces for versions older than Debian Bullseye.
  * Drop unused autopkgtest leftovers.
  * Explicitly pass -DBUILD_TESTING=ON to cmake: this way test-only symbols
    are exposed even when $DEB_BUILD_OPTIONS contains "nocheck" (which adds
    -DBUILD_TESTING=OFF as parameter to cmake). (Closes: #1086783)
  * Drop the unused ${shlibs:Depends} substvar from libkf5xmlrpcclient-data.

  [ Rebecca N. Palmer ]
  * d/watch: Look in KDE 5, as this package is not in KDE 6.

 -- Pino Toscano <pino@debian.org>  Wed, 26 Feb 2025 11:24:33 +0100

kxmlrpcclient (5.115.0-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Sun, 05 May 2024 00:16:32 +0200

kxmlrpcclient (5.115.0-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.115.0).
  * Update build-deps and deps with the info from cmake.
  * Add myself as uploader and remove inactive ones. Thank you for your
    work.

 -- Patrick Franz <deltaone@debian.org>  Tue, 19 Mar 2024 00:07:30 +0100

kxmlrpcclient (5.107.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.105.0).
  * Update build-deps and deps with the info from cmake.
  * New upstream release (5.107.0).
  * Update build-deps and deps with the info from cmake.
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 18 Jun 2023 16:08:46 +0200

kxmlrpcclient (5.104.0-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.104.0).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 15 Mar 2023 16:10:03 +0100

kxmlrpcclient (5.103.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.103.0).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 12 Feb 2023 21:44:32 +0100

kxmlrpcclient (5.102.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.102.0).
  * Update build-deps and deps with the info from cmake.
  * Bump Standards-Version to 4.6.2, no change required.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 22 Jan 2023 21:35:10 +0100

kxmlrpcclient (5.101.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.101.0).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 13 Dec 2022 07:40:36 +0100

kxmlrpcclient (5.100.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.99.0).
  * Update build-deps and deps with the info from cmake.
  * New upstream release (5.100.0).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 19 Nov 2022 23:19:06 +0100

kxmlrpcclient (5.98.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.98.0).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 18 Sep 2022 23:13:10 +0200

kxmlrpcclient (5.97.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.97.0).

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 14 Aug 2022 18:55:41 +0200

kxmlrpcclient (5.96.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.96.0).
  * Bump Standards-Version to 4.6.1, no change required.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 31 Jul 2022 13:33:10 +0200

kxmlrpcclient (5.94.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.94.0).

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 19 May 2022 23:59:07 +0200

kxmlrpcclient (5.93.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.93.0).
  * Update the list of installed files from build logs.

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 11 May 2022 23:22:48 +0200

kxmlrpcclient (5.90.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.89.0).

  [ Aurélien COUDERC ]
  * New upstream release (5.90.0).
  * Added myself to the uploaders.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 11 Feb 2022 23:46:45 +0100

kxmlrpcclient (5.88.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.87.0).
  * New upstream release (5.88.0).
  * Force-bump frameworks internal b-d to 5.88.

 -- Norbert Preining <norbert@preining.info>  Wed, 17 Nov 2021 06:07:42 +0900

kxmlrpcclient (5.86.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.86.0).
  * Bump Standards-Version to 4.6.0, no change required.

 -- Norbert Preining <norbert@preining.info>  Mon, 13 Sep 2021 11:02:33 +0900

kxmlrpcclient (5.85.0-2) unstable; urgency=medium

  * Release to unstable.

 -- Norbert Preining <norbert@preining.info>  Sat, 28 Aug 2021 23:42:49 +0900

kxmlrpcclient (5.85.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.85.0).

 -- Norbert Preining <norbert@preining.info>  Mon, 16 Aug 2021 00:06:35 +0900

kxmlrpcclient (5.83.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Norbert Preining <norbert@preining.info>  Mon, 16 Aug 2021 11:53:11 +0900

kxmlrpcclient (5.83.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.83.0).

 -- Norbert Preining <norbert@preining.info>  Sun, 13 Jun 2021 09:26:20 +0900

kxmlrpcclient (5.82.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.82.0).
  * Removed Maximiliano Curia from the uploaders, thanks for your work
    on the package!

 -- Norbert Preining <norbert@preining.info>  Sat, 08 May 2021 21:22:33 +0900

kxmlrpcclient (5.81.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream version (5.81.0)
  * Bump inter-frameworks dependencies.

 -- Norbert Preining <norbert@preining.info>  Mon, 12 Apr 2021 10:18:21 +0900

kxmlrpcclient (5.80.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream version (5.80.0)
  * Bump inter-frameworks dependencies.

 -- Norbert Preining <norbert@preining.info>  Mon, 15 Mar 2021 15:10:36 +0900

kxmlrpcclient (5.79.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * Remove graphviz from build depends.
  * New upstream release (5.79.0).

 -- Norbert Preining <norbert@preining.info>  Tue, 16 Feb 2021 06:45:30 +0900

kxmlrpcclient (5.78.0-2) unstable; urgency=medium

  * Release to unstable.

 -- Norbert Preining <norbert@preining.info>  Sun, 17 Jan 2021 12:02:26 +0900

kxmlrpcclient (5.78.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.78.0).
  * Update build-deps and deps with the info from cmake.

 -- Norbert Preining <norbert@preining.info>  Wed, 13 Jan 2021 10:32:44 +0900

kxmlrpcclient (5.77.0-2) unstable; urgency=medium

  * Release to unstable.

 -- Norbert Preining <norbert@preining.info>  Tue, 22 Dec 2020 10:33:37 +0900

kxmlrpcclient (5.77.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * Bump Standards-Version to 4.5.1 (No changes needed).
  * New upstream release (5.77.0).
  * Update build-deps and deps with the info from cmake.
  * Add myself to Uploaders.
  * Update maintainer to Debian Qt/KDE Maintainers.

 -- Norbert Preining <norbert@preining.info>  Fri, 18 Dec 2020 10:03:56 +0900

kxmlrpcclient (5.74.0-2) unstable; urgency=medium

  * Team upload to unstable.

 -- Sandro Knauß <hefee@debian.org>  Mon, 19 Oct 2020 23:18:26 +0200

kxmlrpcclient (5.74.0-1) experimental; urgency=medium

  * Team upload.

  [ Sandro Knauß ]
  * New upstream release (5.74.0).
  * kxmlrpcclient moved to portingAids.
  * Update build-deps and deps with the info from cmake.
  * Update Homepage link to point to new invent.kde.org
  * Update repository related entries to metadata file.
  * Update field Source in debian/copyright to invent.kde.org move.
  * Add Bug-* entries to metadata file.

 -- Sandro Knauß <hefee@debian.org>  Sun, 27 Sep 2020 17:46:30 +0200

kxmlrpcclient (5.70.0-1) unstable; urgency=medium

  * Team upload.

  [ Sandro Knauß ]
  * Bump compat level to 13.
  * New upstream release (5.70.0).
  * Update build-deps and deps with the info from cmake.
  * Wrap long lines in changelog entries: 5.22.0-1.
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Sandro Knauß <hefee@debian.org>  Tue, 26 May 2020 23:58:12 +0200

kxmlrpcclient (5.69.0-1) experimental; urgency=medium

  * Team upload.

  [ Sandro Knauß ]
  * Bump compat level to 12.
  * Bump Standards-Version to 4.5.0 (No changes needed).
  * Get rid of debug-symbol-migration package.
  * Add Rules-Requires-Root field to control.
  * New upstream release (5.69.0).
  * Update build-deps and deps with the info from cmake.
  * Add Build-Depends-Package to symbols file.
  * Enable hardening all.
  * Cleanup copyright file.
  * Remove not needed injection of linker flags.

 -- Sandro Knauß <hefee@debian.org>  Thu, 30 Apr 2020 17:57:01 +0200

kxmlrpcclient (5.62.0-1) unstable; urgency=medium

  * Update reprotest args variable
  * New upstream release (5.62.0).
  * Update build-deps and deps with the info from cmake
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Fri, 20 Sep 2019 10:13:15 -0700

kxmlrpcclient (5.61.0-1) experimental; urgency=medium

  [ Scarlett Moore ]
  * Build-Depend on qttools5-dev to fix a cross build failure
    (see #915122 for more info) (Closes: 933022).

  [ Maximiliano Curia ]
  * Salsa CI automatic initialization by Tuco
  * New upstream release (5.61.0).
  * Update build-deps and deps with the info from cmake
  * Add missing version to maintscript
  * Ignore reprotest build path variations
  * Release to experimental

  [ Jonathan Riddell ]
  * moved qdebug files

 -- Maximiliano Curia <maxy@debian.org>  Mon, 09 Sep 2019 21:11:05 +0100

kxmlrpcclient (5.54.0-1) unstable; urgency=medium

  * New upstream release (5.52.0).
  * Update build-deps and deps with the info from cmake
  * New upstream release (5.53.0).
  * Update build-deps and deps with the info from cmake
  * New upstream release (5.54.0).
  * Update build-deps and deps with the info from cmake
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Thu, 17 Jan 2019 19:27:31 -0300

kxmlrpcclient (5.51.0-1) unstable; urgency=medium

  * New upstream release (5.50.0).
  * Update build-deps and deps with the info from cmake
  * New upstream release (5.51.0).
  * Update build-deps and deps with the info from cmake
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Wed, 07 Nov 2018 17:17:29 +0100

kxmlrpcclient (5.49.0-1) unstable; urgency=medium

  * New upstream release (5.48.0).
  * Update build-deps and deps with the info from cmake
  * New upstream release (5.49.0).
  * Update build-deps and deps with the info from cmake
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Fri, 17 Aug 2018 16:19:09 +0200

kxmlrpcclient (5.47.0-1) unstable; urgency=medium

  * New upstream release (5.47.0).
  * Update build-deps and deps with the info from cmake
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Fri, 15 Jun 2018 12:10:47 +0200

kxmlrpcclient (5.46.0-1) unstable; urgency=medium

  * New upstream release (5.46.0).
  * Update build-deps and deps with the info from cmake
  * Bump Standards-Version to 4.1.4.
  * Use https for the debian/copyright
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Thu, 17 May 2018 22:15:45 +0200

kxmlrpcclient (5.45.0-1) unstable; urgency=medium

  * New upstream release (5.45.0).
  * Update build-deps and deps with the info from cmake
  * Update install files
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Sat, 05 May 2018 08:11:06 +0200

kxmlrpcclient (5.44.0-1) sid; urgency=medium

  * New upstream release (5.44.0).
  * Update build-deps and deps with the info from cmake
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Wed, 21 Mar 2018 14:41:26 +0100

kxmlrpcclient (5.43.0-1) experimental; urgency=medium

  * Use the salsa canonical urls
  * New upstream release (5.43.0).
  * Update build-deps and deps with the info from cmake
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Mon, 26 Feb 2018 11:44:16 +0100

kxmlrpcclient (5.42.0-2) sid; urgency=medium

  * New revision
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Sat, 10 Feb 2018 11:29:19 +0100

kxmlrpcclient (5.42.0-1) experimental; urgency=medium

  * New upstream release (5.42.0).
  * Add link options as-needed
  * Bump debhelper build-dep and compat to 11.
  * Build without build_stamp
  * Update build-deps and deps with the info from cmake
  * Bump Standards-Version to 4.1.3.
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Fri, 02 Feb 2018 12:06:06 +0100

kxmlrpcclient (5.41.0-1) experimental; urgency=medium

  * New upstream release (5.41.0).
  * Bump Standards-Version to 4.1.2.
  * Update build-deps and deps with the info from cmake
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Fri, 15 Dec 2017 10:43:17 -0300

kxmlrpcclient (5.41.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 08 Dec 2017 14:48:56 +0000

kxmlrpcclient (5.40.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 10 Nov 2017 13:00:49 +0000

kxmlrpcclient (5.39.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sun, 15 Oct 2017 11:47:53 +0000

kxmlrpcclient (5.38.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 11 Sep 2017 13:32:14 +0000

kxmlrpcclient (5.37.0-2) sid; urgency=medium

  * New revision
  * Bump Standards-Version to 4.1.0.
  * Update upstream metadata
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Sun, 03 Sep 2017 09:59:02 +0200

kxmlrpcclient (5.37.0-1) experimental; urgency=medium

  * New upstream release (5.37.0).
  * Update build-deps and deps with the info from cmake
  * Add a libkf5xmlrpcclient-doc package for the qch files
  * Bump Standards-Version to 4.0.1.
  * Set section for the doc package
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Wed, 16 Aug 2017 10:43:44 +0200

kxmlrpcclient (5.37.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 11 Aug 2017 15:18:36 +0000

kxmlrpcclient (5.36.0-1) experimental; urgency=medium

  [ Maximiliano Curia ]
  * New upstream release (5.35.0).
  * Bump Standards-Version to 4.0.0.
  * Update build-deps and deps with the info from cmake
  * watch: Use https uris
  * New upstream release (5.36.0).
  * Update build-deps and deps with the info from cmake
  * Update symbols files.

  [ Raymond Wooninck ]
  * Add signing key
  * also include signing-key

  [ Harald Sitter ]
  * acc c++11. now used more liberally

 -- Maximiliano Curia <maxy@debian.org>  Sun, 09 Jul 2017 23:42:34 +0200

kxmlrpcclient (5.36.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 10 Jul 2017 17:15:24 +0000

kxmlrpcclient (5.35.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sun, 11 Jun 2017 20:38:04 +0000

kxmlrpcclient (5.34.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sun, 14 May 2017 17:40:29 +0000

kxmlrpcclient (5.33.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 07 Apr 2017 16:26:14 +0000

kxmlrpcclient (5.32.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sun, 12 Mar 2017 13:23:53 +0000

kxmlrpcclient (5.31.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 13 Feb 2017 14:41:38 +0000

kxmlrpcclient (5.30.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 16 Jan 2017 13:44:02 +0000

kxmlrpcclient (5.29.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 09 Dec 2016 19:09:27 +0000

kxmlrpcclient (5.28.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

  [ Maximiliano Curia ]
  * New upstream release (5.28)

 -- Maximiliano Curia <maxy@debian.org>  Fri, 18 Nov 2016 16:06:37 +0100

kxmlrpcclient (5.28.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 17 Nov 2016 09:46:47 +0000

kxmlrpcclient (5.27.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

  [ Maximiliano Curia ]
  * New upstream release (5.27)

 -- Maximiliano Curia <maxy@debian.org>  Sat, 15 Oct 2016 17:07:11 +0200

kxmlrpcclient (5.27.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sat, 08 Oct 2016 11:33:42 +0000

kxmlrpcclient (5.26.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

 -- Maximiliano Curia <maxy@debian.org>  Thu, 29 Sep 2016 12:02:17 +0200

kxmlrpcclient (5.26.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 12 Sep 2016 08:34:11 +0000

kxmlrpcclient (5.25.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

 -- Maximiliano Curia <maxy@debian.org>  Sat, 20 Aug 2016 16:47:16 +0200

kxmlrpcclient (5.25.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sat, 13 Aug 2016 20:16:40 +0000

kxmlrpcclient (5.24.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 11 Jul 2016 07:42:57 +0000

kxmlrpcclient (5.23.0-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

 -- Maximiliano Curia <maxy@debian.org>  Wed, 22 Jun 2016 18:11:55 +0200

kxmlrpcclient (5.23.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 13 Jun 2016 09:40:02 +0000

kxmlrpcclient (5.22.0-1) unstable; urgency=medium

  [ Maximiliano Curia ]
  * Replace the "Historical name" ddeb-migration by its "Modern, clearer"
    replacement dbgsym-migration.
  * Add upstream metadata (DEP-12)
  * debian/control: Update Vcs-Browser and Vcs-Git fields
  * Update acc test script

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake
  * Bump Standards-Version to 3.9.8

 -- Maximiliano Curia <maxy@debian.org>  Wed, 25 May 2016 12:06:57 +0200

kxmlrpcclient (5.22.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 17 May 2016 07:37:12 +0000

kxmlrpcclient (5.21.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 11 Apr 2016 09:24:11 +0000

kxmlrpcclient (5.20.0-0neon) wily; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 01 Apr 2016 11:08:43 +0000

kxmlrpcclient (5.19.0-1) experimental; urgency=medium

  * New upstream release (5.19.0).

 -- Maximiliano Curia <maxy@debian.org>  Sat, 13 Feb 2016 15:16:32 +0100

kxmlrpcclient (5.18.0-1) experimental; urgency=medium

  * New upstream release (5.17.0).
  * New upstream release (5.18.0).

 -- Maximiliano Curia <maxy@debian.org>  Wed, 27 Jan 2016 13:34:26 +0100

kxmlrpcclient (5.16.0-1) unstable; urgency=medium

  * New upstream release (5.16.0).

 -- Maximiliano Curia <maxy@debian.org>  Mon, 30 Nov 2015 12:12:20 +0100

kxmlrpcclient (5.15.0-1) unstable; urgency=medium

  * New upstream release (5.15.0).

 -- Maximiliano Curia <maxy@debian.org>  Fri, 09 Oct 2015 19:16:43 +0200

kxmlrpcclient (5.15.0-0ubuntu2) xenial; urgency=medium

  * Fix autopackagetests by adding allow-stderr to the requirements.

 -- Philip Muškovac <yofel@kubuntu.org>  Tue, 03 Nov 2015 15:34:51 +0100

kxmlrpcclient (5.15.0-0ubuntu1) wily; urgency=medium

  [ Scarlett Clark ]
  * Vivid backport

  [ Jonathan Riddell ]
  * new upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Wed, 07 Oct 2015 13:58:49 +0100

kxmlrpcclient (5.14.0-1) unstable; urgency=medium

  * New upstream release (5.14.0).
  * Removed private classes from exported symbols.

 -- Maximiliano Curia <maxy@debian.org>  Tue, 15 Sep 2015 13:49:45 +0200

kxmlrpcclient (5.14.0-0ubuntu1) wily; urgency=medium

  * new upstream release
  * Fix symbols file

 -- Clive Johnston <clivejo@aol.com>  Thu, 17 Sep 2015 13:28:37 +0100

kxmlrpcclient (5.13.0-1) unstable; urgency=medium

  * New upstream release (5.13.0).

 -- Maximiliano Curia <maxy@debian.org>  Tue, 01 Sep 2015 00:17:19 +0200

kxmlrpcclient (5.13.0-0ubuntu1) wily; urgency=medium

  * new upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Mon, 10 Aug 2015 13:35:08 +0200

kxmlrpcclient (5.12.0-1) unstable; urgency=medium

  * New upstream release (5.12.0).

 -- Maximiliano Curia <maxy@debian.org>  Thu, 09 Jul 2015 12:43:59 +0200

kxmlrpcclient (5.12.0-0ubuntu1) wily; urgency=medium

  * New upstream release
  * Fix merrge, remove unused broken backport.
  * Vivid backport.

 -- Scarlett Clark <sgclark@kubuntu.org>  Mon, 03 Aug 2015 16:11:28 +0200

kxmlrpcclient (5.11.0-1) unstable; urgency=medium

  [ Jonathan Riddell ]
  * Remove 0001-add-LGPL-file.patch, applied upstream

  [ Maximiliano Curia ]
  * New upstream release (5.11.0).

 -- Maximiliano Curia <maxy@debian.org>  Mon, 29 Jun 2015 10:56:14 +0200

kxmlrpcclient (5.10.0-0ubuntu1) wily; urgency=medium

  [ Maximiliano Curia ]
  * New upstream release (5.10.0).
  * New upstream patch: 0001-add-LGPL-file.patch

  [ Jonathan Riddell ]
  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Wed, 03 Jun 2015 20:35:40 +0200

kxmlrpcclient (5.9.0-1) experimental; urgency=medium

  * New upstream release (5.9.0).
  * Add missing Breaks Replaces.

 -- Maximiliano Curia <maxy@debian.org>  Thu, 23 Apr 2015 08:26:15 +0200

kxmlrpcclient (5.9.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Scarlett Clark <sgclark@kubuntu.org>  Mon, 13 Apr 2015 22:25:09 +0200

kxmlrpcclient (5.8.0-1) experimental; urgency=medium

  [ Harald Sitter ]
  * Split from plasma-workspace. kxmlrpcclient is now a framework.

  [ Jonathan Riddell ]
  * New upstream release

  [ Scarlett Clark ]
  * usr/share/locale seems to have gone missing.
    - comment out data package as it would be empty.
  * Remove the depend to empty -data package.
  * Split from plasma-workspace. kxmlrpcclient is now a framework.
  * Drop epoch as there is no file or package name overlap with the old
    library.

  [ Maximiliano Curia ]
  * Prepare initial Debian release.

 -- Maximiliano Curia <maxy@debian.org>  Sun, 22 Mar 2015 11:38:32 +0100

kxmlrpcclient (5.8.0-0ubuntu1) vivid; urgency=medium

  [ Harald Sitter ]
  * Split from plasma-workspace. kxmlrpcclient is now a framework.

  [ Jonathan Riddell ]
  * New upstream release

  [ Scarlett Clark ]
  * usr/share/locale seems to have gone missing.
    - comment out data package as it would be empty.
  * Remove the depend to empty -data package.

 -- Jonathan Riddell <jriddell@ubuntu.com>  Fri, 13 Mar 2015 20:55:38 +0100
